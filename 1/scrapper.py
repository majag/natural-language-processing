#!/usr/bin/env python
# -*- coding: utf-8 -*-

from urllib2 import urlopen
from bs4 import BeautifulSoup
import re

url="http://www.forum.hr/forumdisplay.php?f=15"
content=urlopen(url).read()
soup = BeautifulSoup(content)

# svi podpodforumi unutar podforuma hardver i softver
it = soup.find_all('td', {"class": "alt1Active"})
it_podteme = []


for p in it:
	it_podteme.append(p.find('a').get('href'))

#print it_podteme


# uzima sve topice unutar podforuma it

it_topics = []
it_topics_title = []

for p in it_podteme:
	url2 = "http://www.forum.hr/" + p
	content2 = urlopen(url2).read()
	soup2 = BeautifulSoup(content2)
	for title in soup2.find_all('a', attrs={'id' : re.compile("thread_title")}):
		it_topics_title.append(str(title.contents).replace('\u0160', 'S').replace('\u0161', 's').replace('\u010c', 'C').replace('\u010d', 'c').replace('\u0106', 'C').replace('\u0107', 'c').replace('\u0110', 'D').replace('\u0111', 'D').replace('\u017d', 'Z').replace('\u017e', 'z'))
		it_topics.append(title.get('href'))
			

# preuzimanje postova iz topica

j = 0

for post in it_topics:
	rbr = 0					#redni broj posta u topicu
	url3 = "http://www.forum.hr/" + post
	content3 = urlopen(url3, timeout=180000).read()
	soup3 = BeautifulSoup(content3)
	all_posts = soup3.find_all('td', attrs={'id' : re.compile("td_post_")})
	it_dates_all = soup3.find_all(text=re.compile('\d{2}[.]\d{2}[.]\d{4}\.,[ ]\d{2}:\d{2}'))
	it_dates_all += soup3.find_all(text=re.compile('\D{5},[ ]\d{2}:\d{2}'))
	it_dates_all = (((str(it_dates_all).replace('\\r\\n\\t\\t\\t','')[3:-2]).replace('u\'','').replace('\\u010d','c').replace('.,', '').replace('.', '-')).split(' '))[0::2]
	#print it_dates_all
	for p in all_posts:
		it_posts_array = []
		it_posts_array.append((p.find('div', attrs={'id' : re.compile("post_message")}).get_text()).replace('\n',''))
		for item in it_posts_array:
			length = len(item)
			if (length > 900):
				f = open("it/pos_Forum-hr-" + str(it_topics_title[j]).replace('/', ' ')[3:-2] + "_" + str(it_dates_all[rbr]) + "_" + str(rbr) + ".txt", 'w')
				f.write(item.encode('utf-8'))
				f.close()
		rbr+=1
	# while (soup4.find('a', {"title": re.compile("stranica - Rezultati")})):
	# 	rbrIn=0
	# 	url31 = "http://www.forum.hr/" + ((soup4.find('a', {"title": re.compile("stranica - Rezultati")})).get('href'))
	# 	content31 = urlopen(url3).read()
	# 	soup31 = BeautifulSoup(content3)
	# 	all_posts = soup31.find_all('td', attrs={'id' : re.compile("td_post_")})
	# 	it_dates_all = soup31.find_all(text=re.compile('\d{2}[.]\d{2}[.]\d{4}\.,[ ]\d{2}:\d{2}'))
	# 	it_dates_all += soup31.find_all(text=re.compile('\D{5},[ ]\d{2}:\d{2}'))
	# 	it_dates_all = (((str(it_dates_all).replace('\\r\\n\\t\\t\\t','')[3:-2]).replace('u','').replace('\\010d','uc').replace(',','')).split(' '))[0::2]
	# 	#print it_dates_all
	# 	for p in all_posts:
	# 		it_posts_array = []
	# 		it_posts_array.append((p.find('div', attrs={'id' : re.compile("post_message")}).get_text()).replace('\n',''))
	# 		for item in it_posts_array:
	# 			if (len(item)>200):
	# 				f = open("posts/it/" + str(it_topics_title[j]).replace('/', ' ')[3:-2] + "_" + str(it_dates_all[rbrIn]) + "_" + str(rbr) + ".txt", 'w')
	# 				f.write(item.encode('utf-8'))
	# 				f.close()
	# 		rbrIn+=1
	# 		rbr+=1
	j+=1




