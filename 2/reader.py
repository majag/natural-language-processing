
def create_dict(dat):
    f = open(dat, 'r')

    global pos_dict
    global neg_dict
    global svi_dict    
		
    pos_dict={}
    neg_dict={}
    svi_dict={}

    for line in f:
        line = line.strip()
        redak = line.split()
        eng = redak[0]
        hrv = redak[1]
        pol = redak[2]

        #za hrvatski korpus
        if int(pol)<0:
            pol = abs(int(pol))
            neg_dict[hrv] = pol
            param = "neg"
            svi_dict[hrv] = (param, pol)
        else:
            pos_dict[hrv] = pol     
            param = "pos"
            svi_dict[hrv] = (param, pol)
    
    #ispis rjecnika u datoteku
    #f2 = open('output.txt', 'w')
    #f2.write("pos_dict = " + str(pos_dict) + "\n\nneg_dict = " + str(neg_dict) + "\n\nsvi_dict = " + str(svi_dict))
    #f2.close()

create_dict('../2/leksicki_resurs.txt')
