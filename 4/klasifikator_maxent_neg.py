#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
import random
import os
import sys
import fnmatch
from nltk.corpus.reader import CategorizedPlaintextCorpusReader
from nltk.classify.maxent import MaxentClassifier

#uključivanje funkcija iz zadatka 3
sys.path.insert(0, '/home/maja/NLP/3/')
from feature_extractors import features_neg

file_list = (os.listdir('../1/it'))      #svi komentari tj sve datoteke 
file_cat = {}
file_cat_pos={}
file_cat_neg={}

for filenum in range(0,len(file_list)):
    if fnmatch.fnmatch(file_list[filenum], 'pos*'):
        file_cat_pos.update({file_list[filenum]:['pos']})
        file_cat.update({file_list[filenum]:['pos']})
    else:
        file_cat_neg.update({file_list[filenum]:['neg']})
        file_cat.update({file_list[filenum]:['neg']})
       
root_dir = '../1/'
file_ids = r'\..\/1/it/.*\.txt'
fileid = file_ids
category = file_cat

reader = CategorizedPlaintextCorpusReader(root_dir+'/it', r'.*\.txt', cat_map=file_cat)
# reader.categories() 
# reader.fileids(categories=['neg']) 
# reader.fileids(categories=['pos']) 

files = [(list(reader.words(fileid)), category) for category in reader.categories() for fileid in reader.fileids(category)]
random.shuffle(files)

neg_features = []
neg_features = [(features_neg(d), c) for (d,c) in files]

granica = int(len(files)*0.9)
train_set = neg_features[:granica]
test_set = neg_features[granica:]

dat = open('klasifikator_maxent_neg_rezultat.txt', 'w')

classifier = nltk.MaxentClassifier.train(train_set)
classifier.show_most_informative_features(15)

dat.write("Maxent accuracy:" + str((nltk.classify.accuracy(classifier, test_set))*100) + "%")

dat.close()