#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
import random
import os
import sys
import fnmatch
from nltk.corpus.reader import CategorizedPlaintextCorpusReader
from nltk.classify.decisiontree import DecisionTreeClassifier

#uključivanje funkcija iz zadatka 3
sys.path.insert(0, '/home/maja/NLP/3/')
from feature_extractors import features_all

file_list = (os.listdir('../1/it'))      #svi komentari tj sve datoteke 
file_cat = {}
file_cat_pos={}
file_cat_neg={}

for filenum in range(0,len(file_list)):
    if fnmatch.fnmatch(file_list[filenum], 'pos*'):
        file_cat_pos.update({file_list[filenum]:['pos']})
        file_cat.update({file_list[filenum]:['pos']})
    else:
        file_cat_neg.update({file_list[filenum]:['neg']})
        file_cat.update({file_list[filenum]:['neg']})
       
root_dir = '../1/'
file_ids = r'\..\/1/it/.*\.txt'
fileid = file_ids
category = file_cat

reader = CategorizedPlaintextCorpusReader(root_dir+'/it', r'.*\.txt', cat_map=file_cat)
# reader.categories() 
# reader.fileids(categories=['neg']) 
# reader.fileids(categories=['pos']) 

files = [(list(reader.words(fileid)), category) for category in reader.categories() for fileid in reader.fileids(category)]
random.shuffle(files)

all_features = []
all_features = [(features_all(d), c) for (d,c) in files]

granica = int(len(files)*0.9)
train_set = all_features[:granica]
test_set = all_features[granica:]

dat = open('klasifikator_tree_all_rezultat.txt', 'w')

classifier = nltk.DecisionTreeClassifier.train(train_set)
#print classifier # ispiše značajke ali ih ne sprema u datoteku, kao i kad koristimo show_most_informative_feature

'''
Return a string representation of this decision tree that expresses the decisions it makes 
as a nested set of pseudocode if statements
'''
most_informative_features = classifier.pseudocode()

dat.write("Decision Tree accuracy:" + str((nltk.classify.accuracy(classifier, test_set))*100) + "%\n\nMost Informative Features: \n\n" + str(most_informative_features))

dat.close()