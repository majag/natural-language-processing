#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
import random
import os
import sys
import fnmatch
from nltk.corpus.reader import CategorizedPlaintextCorpusReader
from nltk.classify.naivebayes import NaiveBayesClassifier

#uključivanje funkcija iz zadatka 3
sys.path.insert(0, '/home/maja/NLP/3/')
from feature_extractors import features_pos

file_list = (os.listdir('../1/it'))      #svi komentari tj sve datoteke 
file_cat = {}
file_cat_pos={}
file_cat_neg={}

for filenum in range(0,len(file_list)):
    if fnmatch.fnmatch(file_list[filenum], 'pos*'):
        file_cat_pos.update({file_list[filenum]:['pos']})
        file_cat.update({file_list[filenum]:['pos']})
    else:
        file_cat_neg.update({file_list[filenum]:['neg']})
        file_cat.update({file_list[filenum]:['neg']})
       
root_dir = '../1/'
file_ids = r'\..\/1/it/.*\.txt'
fileid = file_ids
category = file_cat

reader = CategorizedPlaintextCorpusReader(root_dir+'/it', r'.*\.txt', cat_map=file_cat)
# reader.categories() 
# reader.fileids(categories=['neg']) 
# reader.fileids(categories=['pos']) 

files = [(list(reader.words(fileid)), category) for category in reader.categories() for fileid in reader.fileids(category)]
random.shuffle(files)

pos_features = []
pos_features = [(features_pos(d), c) for (d,c) in files]

granica = int(len(files)*0.9)
train_set = pos_features[:granica]
test_set = pos_features[granica:]

dat = open('klasifikator_bayes_pos_rezultat.txt', 'w')

classifier = nltk.NaiveBayesClassifier.train(train_set)

# def show_most_informative_features(self, n=15):
#     strlist = []
#     # Determine the most relevant features, and display them.
#     cpdist = self._feature_probdist
#     # print('Most Informative Features')
#     strlist.append('Most Informative Features')
#     for (fname, fval) in self.most_informative_features(n):
#         def labelprob(l):
#             return cpdist[l,fname].prob(fval)
#         labels = sorted([l for l in self._labels
#                  if fval in cpdist[l,fname].samples()],
#                 key=labelprob)
#         if len(labels) == 1: continue
#         l0 = labels[0]
#         l1 = labels[-1]
#         if cpdist[l0,fname].prob(fval) == 0:
#             ratio = 'INF'
#         else:
#             ratio = '%8.1f' % (cpdist[l1,fname].prob(fval) /
#                       cpdist[l0,fname].prob(fval))
#         # print(('%24s = %-14r %6s : %-6s = %s : 1.0' %
#             #      (fname, fval, ("%s" % l1)[:6], ("%s" % l0)[:6], ratio)))
#         strlist.append(('%24s = %-14r %6s : %-6s = %s : 1.0' %
#                       (fname, fval, ("%s" % l1)[:6], ("%s" % l0)[:6], ratio)))
#     return strlist

#lista = show_most_informative_features(classifier, 15)
#dat.write("Bayes accuracy:" + str((nltk.classify.accuracy(classifier, test_set))*100) + "%\n\n" + str(lista))

classifier.show_most_informative_features(15)
most_informative_features = classifier.most_informative_features(15)

dat.write("Bayes accuracy:" + str((nltk.classify.accuracy(classifier, test_set))*100) + "%\n\nMost Informative Features: \n\n" + str(most_informative_features))

dat.close()

#Izračun jedinstvenih riječi

#ukupno_rijeci = len(reader.words())
#pojavnice = len(set(reader.words()))