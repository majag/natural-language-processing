#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
import sys
sys.path.insert(0, '../2/')
from reader import create_dict

#proizvoljno izabran komentar za provjeru koda, u ovom slučaju radi se o najvećem komentaru
f = open("../1/it/pos_Forum-hr-Tutorial - BitTorrent_16-02-2009_1.txt")
text1 = f.read()
text_split = text1.split()
text = [x.strip(',:;.?!()/*').lower() for x in text_split]

from reader import pos_dict
from reader import neg_dict
from reader import svi_dict

def features_pos(text):	
	result_pos={}
	for key, value in pos_dict.iteritems():
		br_pojavnica = text.count(key)
		nova_vrijednosti = int(value) * br_pojavnica
		result_pos[key] = nova_vrijednosti
	return result_pos

def features_neg(text):	
	result_neg={}
	for key, value in neg_dict.iteritems():
		br_pojavnica = text.count(key)
		nova_vrijednosti = int(value) * br_pojavnica
		result_neg[key] = nova_vrijednosti
	return result_neg
	
def features_all(text):	
	result_all={}
	for key, value in svi_dict.iteritems():
		br_pojavnica = text.count(key)
		nova_vrijednosti = int(value[1]) * br_pojavnica
		result_all[key + "_" + value[0]] = nova_vrijednosti
	return result_all

#print "features_pos = " + str(features_pos(text)) + "\n\nfeatures_neg = " + str(features_neg(text)) + "\n\nfeatures_all = " + str(features_all(text)) 

#ispis rjecnika u datoteku
# f2 = open('features_pos.txt', 'w')
# f2.write(str(features_pos(text)))
# f2.close()
# f2 = open('features_neg.txt', 'w')
# f2.write(str(features_neg(text)))
# f2.close()
# f2 = open('features_all.txt', 'w')
# f2.write(str(features_all(text)))
# f2.close()