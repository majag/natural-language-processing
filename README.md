# Natural language processing project

1. In the first part of the project a corpus was made. A scrapper.py script was made for automatic downloading of the content from an online forum, and for cleaning the corpus. Each comment is saved as a separate txt file and categorized into mostly positive or mostly negative
The description of the corpus is given in the opis_korpusa.txt file.

2. In the second part of the project a script was made to read the polarities from the leksicki_resurs.txt file. The polarities were supposed to be separated into two parts by using the Python dictionary for positive and negative polarity. 

3. In the third part functions for the extraction of features were written. This includes three functions - for negative, positive and all features. 

4. In the last part of the project comments were classified by polarity using three different classifiers: NaiveBayes, DecisionTree and Maxent classifier. 